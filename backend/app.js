// backend/app.js
const express = require('express');
const mongoose = require('mongoose');
const socketio = require('socket.io');
const http = require('http');
const path = require('path'); // Import path module
const chatRoutes = require('./routes/chatRoute.js');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

// Middleware
app.use(express.json());
// Serve the socket.io library
app.use('/socket.io', express.static(path.join(__dirname, 'node_modules', 'socket.io', 'client-dist')));

// Database connection
mongoose.connect('mongodb://localhost:27017/chatApp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(() => console.log('Connected to MongoDB'))
.catch(err => console.error('Error connecting to MongoDB:', err));

// Socket.IO logic
io.on('connection', (socket) => {
  console.log('A user connected');
  // Handle events like 'sendMessage' here
  socket.on('disconnect', () => {
    console.log('A user disconnected');
  });
});

// Serve static files for the /chat route
app.use('/chat', express.static(path.join(__dirname, 'public')));

// Routes
app.use('/api/chat', chatRoutes);

// Error handling middleware
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ error: 'Internal server error' });
});

module.exports = server;
