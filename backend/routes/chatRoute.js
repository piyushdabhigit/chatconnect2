// backend/routes/chatRoute.js
const express = require('express');
const router = express.Router();
const Chat = require('../models/chat.js');
const User = require('../models/user.js');

// Helper function to generate a random username (optional)
const generateRandomUsername = () => {
  const randomSuffix = Math.floor(Math.random() * 1000);
  return `user_${randomSuffix}`;
};

// Endpoint for user registration or login
router.post('/registerOrLogin', async (req, res) => {
  try {
    const { username } = req.body;
    let user = await User.findOne({ username });
    if (!user) {
      user = new User({ username });
      await user.save();
    }

    const chat = await Chat.findOne({ participants: user._id });
    if (!chat) {
      return res.status(404).json({ error: 'No chat found' });
    }

    const chatHistory = chat.messages || [];
    res.status(200).json({ history: chatHistory });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Endpoint for connecting users to a chat
router.post('/connect', async (req, res) => {
  try {
    // Create a new user
    const user = new User({});
    await user.save();

    // Find another user to connect with
    const otherUser = await User.findOne({ _id: { $ne: user._id } });
    if (!otherUser) {
      return res.status(404).json({ error: 'No other users available' });
    }

    // Create a new chat between the two users
    const chat = new Chat({
      participants: [user._id, otherUser._id],
      messages: [],
    });
    await chat.save();

    res.status(200).json({ username: user._id });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});


// Endpoint for sending a message in a chat
router.post('/:chatId/send', async (req, res) => {
  try {
    const { chatId } = req.params;
    const { message } = req.body;

    // Find the chat by its ID
    const chat = await Chat.findById(chatId);

    if (!chat) {
      return res.status(404).json({ error: 'Chat not found' });
    }

    // Add the message to the chat
    chat.messages.push(message);
    await chat.save();

    res.status(200).json({ message: 'Message sent successfully' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Endpoint for fetching chat history
router.get('/:chatId/history', async (req, res) => {
  try {
    const { chatId } = req.params;

    // Find the chat by its ID
    const chat = await Chat.findById(chatId);

    if (!chat) {
      return res.status(404).json({ error: 'Chat not found' });
    }

    // Return chat history
    res.status(200).json({ history: chat.messages });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

module.exports = router;
