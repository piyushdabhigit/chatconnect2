// frontend/script.js
const socket = io();
let username = '';
// frontend/script.js
document.addEventListener("DOMContentLoaded", function() {
  const socket = io();
  let username = '';

  socket.on('connect', () => {
    if (!username) {
      document.getElementById('registrationForm').style.display = 'block';
    }
  });

  socket.on('chatHistory', (data) => {
    const chatMessages = document.getElementById('chatMessages');
    chatMessages.innerHTML = '';
    data.history.forEach(message => {
      const messageElement = document.createElement('div');
      messageElement.textContent = message;
      chatMessages.appendChild(messageElement);
    });
    document.getElementById('chatInterface').style.display = 'block';
  });

  function registerOrLogin() {
    const inputUsername = document.getElementById('usernameInput').value.trim();
    if (inputUsername !== '') {
      username = inputUsername;
      socket.emit('registerOrLogin', { username });
    }
  }

  function sendMessage() {
    const messageInput = document.getElementById('messageInput');
    const message = messageInput.value.trim();
    if (message !== '') {
      socket.emit('sendMessage', { message, username });
      messageInput.value = '';
    }
  }

  socket.on('messageReceived', (data) => {
    const { message, sender } = data;
    const chatMessages = document.getElementById('chatMessages');
    const messageElement = document.createElement('div');
    messageElement.textContent = `${sender}: ${message}`;
    chatMessages.appendChild(messageElement);
    chatMessages.scrollTop = chatMessages.scrollHeight;
  });

  // Attach event listener after page load
  const submitButton = document.getElementById('submitButton');
  submitButton.addEventListener('click', registerOrLogin);
});
