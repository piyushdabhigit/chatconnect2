chat-app/
├── backend/
│   ├── models/
│   │   ├── user.js
│   │   └── chat.js
│   ├── routes/
│   │   └── chatRoute.js
│   ├── app.js
│   └── package.json
└── frontend/
    ├── index.html
    ├── styles.css
    └── script.js


api integratin with frontend